/* Service is an interface that exposes the
methods of an implementation whose details have been
distracted away.
*  */
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {


    //create a user
    void createUser(User user);

    Iterable<User> getUsers();
    //Delete a user
    ResponseEntity deleteUser(Long id);
    //Update a user
    ResponseEntity updateUser(Long id, User user);
}
